<!--
 ██████╗ ███████╗██╗   ██╗ ██████╗ ███████╗██╗    ██╗███████╗
██╔═══██╗██╔════╝╚██╗ ██╔╝██╔═══██╗██╔════╝██║    ██║██╔════╝
██║   ██║█████╗   ╚████╔╝ ██║   ██║█████╗  ██║ █╗ ██║███████╗
██║   ██║██╔══╝    ╚██╔╝  ██║   ██║██╔══╝  ██║███╗██║╚════██║
╚██████╔╝███████╗   ██║   ╚██████╔╝███████╗╚███╔███╔╝███████║
 ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚══════╝ ╚══╝╚══╝ ╚══════╝ Powered by @oeyoews
-->

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![Latest Release](https://gitlab.com/oeyoews/dotfiles/-/badges/release.svg)](https://gitlab.com/oeyoews/dotfiles/-/releases)

## [🎉 Welcom](#)


## 🌑 Dependency

<div style="text-align: center">
<p> Hello </p>
<img src="https://cdn.jsdelivr.net/gh/oeyoews/img/dotfilesgraph.png" title="img" alt="img" style="zoom: 100%" width=512 />
</div>

## 🌷 Tips

* fstrim.timer

<!--obrit-->

## [☑️ TODO](docs/TODO.md)

## [🥘 SOFTWARE](docs/softwares.md)

## 💝 Thanks

<a href="https://jb.gg/OpenSource"><img src="https://cdn.jsdelivr.net/gh/oeyoews/img/jb_beam.svg" alt="JetBrains Logo" width="64px"/>
</a>Thanks to [JetBrains](https://jb.gg/OpenSource)  for providing a free open source license for this project.
