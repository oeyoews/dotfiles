# TODO

* vscode multi cursor
- [x] how to remove vscoe hove box(theme cause)
- [ ] how to remove square in vscode
- [ ] todo: update graph
- [ ] doomemacs: use doomemacs later, and split mysel config
  , just like from spacevim to self custom nvim
- [ ] add descript for softwares

## doomemacs_for_markdown

- [ ] config preview md with emacs

## markdownlint

- [ ] markdownlint learn

## emacs_transition

- [ ] use snippets link vim
- [ ] solve multi buffer memory with doomemacs
- [ ] remove trailling space automatically
- config wezterm theme
- how to embed window terminal check
