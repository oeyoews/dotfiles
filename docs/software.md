# software

## npm packages

- lookatme
- pipx
- ungit
- emoji
- live-server(use npm i -g, not in current projects) and have vscode extensions (live server, not others)
- tailwind
- postcss(about tailwind projects)
- git-open
- slideout
- fancybox

## Download

- you-get(fast)
- youtube-dl
- motrix
- aria2

## Picture

- feh

## Games

- bovo

## Pages

- vercel
- github/gitlab
- jsDelivr
- netlify

## Lint

- mdl
- shellcheck

## Doc

- docify
- vuepress
- docsaurus

## Blog

- hexo
- hugo
- halo

<!--* ~~typecho~~-->

## Wiki

- tiddlywiki5

## scriptmanage

- package.json
- makefile

## write

- notion.so
- cnblog
- wolai
- typora
- mubu
- marktext

## office

- onlyoffice
- libreoffice
- ~~wps~~

## idea

- atom material icons
- atom onedark theme
- indent reinbow
- rainbow brackets

## iso

- etcher
- ventoy

## translate

- translate-shell

## browser

- firefox-nightly
- google-chrome
- kiwibrowser

## editor

- neovim
- helix
- typora-free
- marktext
- visual-studio-code-bin
- libreoffice
- emacs

## Fonts

- cascadia
- [maple](https://github.com/subframe7536/Maple-font)
- twemoji to replace noto emoji font
- FiraCode
- DeJaVu
- Fantasque
- JetBrains

## tools

- Excalidraw(draw)
- anki(phone recommendor)
- kookma(video recorder)
- obs
- clementine(music player)
- foxit reader(book reader)
- foliate(reader)
- gimp(ps)
- mpv(fullscreen): video player
- xournal++(draw)
- krita(?)
- tlp and tlp-rdw
- calibre
- stellarium

## terminal

- alacritty(fullscreen)
- kitty
- wezterm
- qterminal

## gnome

<!--* arcmenu-->

- downgrade
- papirus-folders
- gnome-keyring
  - seahorse
- gnome-todo
- gnome locks
- papirus
- gpaste
  - <!--* Clipboard indicator-->
- dash-to-dock
- base-devel
- refresh wifi connections
<!--* bug color-picker-->
- wl-color-picker

<!--* dynamic panel tranxxx-->

## ime

- fcitx5-im
- fcitx5-chinese-addons
- fcitx5-pinyin-zhwiki
- fcitx5-pinyin-sougou(maybe have memory out)
- fcitx5-pinyin-moegirl

- nload
- gotop
- lyx
- delta
- trash-cli
- exa
- colorls
- tmux
- cdate
- gource

> software version control visualization for git repo

- `cloc` count code lines

- jdk-openjdk
- make
- rg(ripgrep)
- ranger

> NOTE: preview img just support kitty terminal

- [tt](https://github.com/lemnos/tt)
- ttyper
- duf
- dust
- fastfetch
- taskbook
- [slides](https://github.com/maaslalani/slides)
- slidev
- btop
- gtop
- gromit-mpx
- tiddlywiki
- tig
- flameshot
- pm2
- appche2-utils
- meld
- dbeaver
- ventoy
- startuml
- git-filter-repo
- oreo-cursors(pacman)
- lx-music-desktop
- wudao-dict
- musicbox
- default.nvim
- latern
- v2raya
- qv2ray
- vim-init
- wemeet-bin
- renovate
- trello
- html-minifier-terser
- viu
- http-server
- gzip
- gnome-power-manager
- [trilium](https://github.com/zadam/trilium)
- gitclone
- hub.fast.git
- MDN
- [?] linuxhello
- pastbin
- reveal.js
- httrack
- iredis

## Favourite

- emacs-talk

- figlet
- lolcate

- v2rayNG
- chezmoi
- mkdocs
- dev-sidecar

- [gollum](): github or gitlab wiki

## pdf

- zathura(fullscreen)
- okular
- [focalboard](https://github.com/mattermost/focalboard): notion replace?

- showmethekey(wayland)
- [screenkey](https://gitlab.com/screenkey/screenkey#interactive-placement)
- rofi rofi-theme-selector
- thunar
- pip-search
- blanket(voice)

## cmd

- pkill
- sysz(fzf for systemctl)

## cdn

- unpkg
- jsdelivr
- atom material icons for web extensions
